<?php
session_start();


include_once("Menu.class.php");
include_once("Config.class.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Accueil</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="style.css" rel="stylesheet" type="text/css"/>
	<style type="text/css">a:link{text-decoration:none}</style>

</head>
<body>
	<?php

	$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	$req = $pdo->prepare("SELECT nomMetier FROM metier WHERE idMetier= ?");
	$req->execute(array($_SESSION['idMetier']));


	?>
	<div id='image'><p><a href="accueil.php"><img src="images/logo.png" alt="logo" /></a></p></div>
	<?php
	foreach  ($req as $row) {
	echo'<h1>'.$row['nomMetier'].'</h1>';
}
$req=null;
	?>
	<?php
	Menu::display($_SESSION['idMetier']);
	?>
   <div>
			<div class="mv-item2"><a href="personnelGestion.php">Ajouter un employé</a></div>
			<?php
				$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
				$req = $pdo->prepare("SELECT idEmploye,nom, prenoms, actif,nomMetier FROM employe as em JOIN metier as met on metier_idMetier = idMetier");
				$req->execute();
				echo <<<EOT
				<table id = "tableauMetier">
					<tr>
						<td>nom</td>
						<td>Prénom</td>
						<td>Poste</td>
						<td>Actif</td>
					</tr>
EOT;
					while ($row=$req->fetch()) {
						echo <<<EOT
						<tr>
							<td> $row[nom] </td>
							<td> $row[prenoms]</td>
							<td> $row[nomMetier]</td>
							<td>
								<a href="personnelGestion.php?type=actif&idEmploye=$row[idEmploye]">$row[actif]</a>
							</td>
							<td>
								<a href="personnelGestion.php?type=edit&idEmploye= $row[idEmploye]">Editer</a>
							</td>
						</tr>
EOT;
					}
echo <<<EOT
		</table>
   </div>
</html>

EOT;
?>
