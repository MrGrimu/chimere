-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 06 Mai 2015 à 11:14
-- Version du serveur :  5.5.41-0+wheezy1
-- Version de PHP :  5.4.39-0+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE SCHEMA IF NOT EXISTS chimere DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE chimere;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `chimere`
--

-- --------------------------------------------------------

--
-- Structure de la table `bijoux`
--

CREATE TABLE IF NOT EXISTS `bijoux` (
  `idBijoux` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `client_idclient` int(11) NOT NULL,
  `dateButtoir` date DEFAULT NULL,
  `dateSortieAtelier` date DEFAULT NULL,
  `dateCommande` date NOT NULL,
  `tempsEstimer` float DEFAULT NULL,
  `prixCalculer` float DEFAULT NULL,
  `prixReel` float DEFAULT NULL,
  `etatBijoux_idetatBijoux` int(11) NOT NULL,
  `prixEstimer` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `bijoux`
--

INSERT INTO `bijoux` (`idBijoux`, `nom`, `client_idclient`, `dateButtoir`, `dateSortieAtelier`, `dateCommande`, `tempsEstimer`, `prixCalculer`, `prixReel`, `etatBijoux_idetatBijoux`, `prixEstimer`) VALUES
(1, 'SWAG', 1, '2015-09-17', NULL, '2015-04-23', 42, NULL, NULL, 3, NULL),
(2, 'METALS!!!!', 2, '2016-04-19', NULL, '2015-04-23', 50, NULL, NULL, 1, NULL),
(3, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(4, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(5, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(6, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(7, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(8, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(9, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(10, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(11, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(12, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(13, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(14, 'fcghjk', 11, '2015-05-20', NULL, '2015-05-01', 45, NULL, NULL, 1, 7789),
(15, 'sfdx', 20, '2015-05-31', NULL, '2015-05-05', 879, NULL, NULL, 1, 798764),
(16, '8976', 1, '2015-05-04', NULL, '2015-05-29', 564, NULL, NULL, 1, 45);

-- --------------------------------------------------------

--
-- Structure de la table `bijoux_has_photos`
--

CREATE TABLE IF NOT EXISTS `bijoux_has_photos` (
  `bijoux_id` int(11) NOT NULL,
  `photos_idphotos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `idClient` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `prenoms` varchar(45) DEFAULT NULL,
  `adresse` varchar(45) DEFAULT NULL,
  `telephoneFixe` varchar(45) DEFAULT NULL,
  `telephonePortable` varchar(45) DEFAULT NULL,
  `adresseMail` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`idClient`, `nom`, `prenoms`, `adresse`, `telephoneFixe`, `telephonePortable`, `adresseMail`) VALUES
(1, 'Norbet', 'Jean-Jacques', '42 rue du yolo', NULL, NULL, NULL),
(2, 'David', 'Mohammed', '53 rue du détonnant', NULL, NULL, NULL),
(3, 'Pate', 'Brandon', '708-6164 Etiam Ave', '04 15 19 04 16', '02 65 86 69 72', 'Sed.nec.metus@vulputateullamcorpermagna.co.uk'),
(4, 'Lowery', 'Kalia', 'CP 175, 4995 Magna Ave', '01 57 18 70 72', '03 72 24 59 82', 'pede@sitametlorem.ca'),
(5, 'Jones', 'Rosalyn', 'CP 849, 466 Et Avenue', '06 91 32 54 67', '03 70 85 83 09', 'cursus.a@quis.ca'),
(6, 'Cortez', 'Tara', '973-5630 Id Rue', '04 64 94 07 15', '08 11 59 21 00', 'suscipit.nonummy.Fusce@tellusjustosit.co.uk'),
(7, 'Blevins', 'Paul', 'CP 507, 3781 Dolor Rd.', '06 74 43 71 55', '01 20 31 21 18', 'ultrices.iaculis.odio@etlacinia.co.uk'),
(8, 'Murphy', 'Hilel', 'CP 647, 5742 Curabitur Ave', '09 62 16 27 65', '02 83 83 64 77', 'convallis@idsapien.co.uk'),
(9, 'Crane', 'Zelenia', 'Appartement 324-2917 Donec Avenue', '02 61 26 56 71', '03 37 40 03 18', 'eget.varius.ultrices@milorem.com'),
(10, 'Nelson', 'Nicholas', 'Appartement 314-4987 Sapien. Rd.', '04 99 96 62 20', '08 16 94 54 17', 'Pellentesque@tinciduntaliquam.edu'),
(11, 'Strickland', 'TaShya', 'Appartement 743-8833 Malesuada Ave', '01 22 74 47 89', '05 21 61 63 22', 'Donec.porttitor.tellus@erat.ca'),
(12, 'Holder', 'Malik', 'CP 508, 8392 Nulla Chemin', '08 87 83 54 61', '05 57 49 38 26', 'non@dapibusquam.org'),
(13, 'Walker', 'Indira', 'CP 611, 6865 Molestie Route', '01 03 49 40 75', '06 04 51 39 30', 'litora.torquent@ametmetus.edu'),
(14, 'Strong', 'Bradley', 'CP 614, 3752 Tellus Chemin', '04 74 79 30 11', '04 56 36 45 13', 'diam.at.pretium@loremauctorquis.net'),
(15, 'Dalton', 'Leah', '105-7960 Amet Rue', '08 65 42 20 48', '07 47 24 95 86', 'lorem.semper.auctor@adlitoratorquent.co.uk'),
(16, 'Vinson', 'Hollee', 'Appartement 556-5454 In Rd.', '01 33 08 87 21', '03 78 89 79 81', 'Nunc.commodo.auctor@vestibulumnec.org'),
(17, 'Reed', 'Shaine', '9161 Sem. Rue', '08 91 74 80 22', '01 75 43 37 50', 'vel.pede.blandit@ut.com'),
(18, 'Tyler', 'Gareth', '297-7536 Urna. Rd.', '08 97 38 99 00', '09 67 87 89 93', 'tortor.nibh.sit@metus.com'),
(19, 'Duffy', 'Baker', 'Appartement 255-2293 Nec Rd.', '03 29 27 53 98', '08 21 67 48 79', 'urna.Vivamus.molestie@eu.co.uk'),
(20, 'Avila', 'Avye', 'Appartement 582-1919 Nibh. Route', '06 83 17 83 03', '09 92 03 39 38', 'elit@sedpede.net'),
(21, 'Webster', 'Amaya', 'CP 700, 7333 Fringilla, Av.', '03 38 82 64 04', '05 38 37 56 37', 'erat.Etiam@non.com'),
(22, 'Vargas', 'Jessamine', '6882 Velit Rue', '07 13 40 78 83', '09 74 69 97 45', 'enim@necmetus.net'),
(23, 'Larsen', 'Uma', 'CP 225, 1183 Lacus. Rue', '01 88 05 88 41', '04 51 40 65 89', 'dapibus.rutrum@eu.edu'),
(24, 'Mcfarland', 'Jermaine', '5419 Sem Rd.', '03 81 65 01 24', '08 12 03 79 43', 'nibh.Phasellus@elitpellentesquea.co.uk'),
(25, 'Rasmussen', 'Tatiana', 'Appartement 151-370 Cursus Route', '06 79 93 77 96', '07 95 56 85 10', 'risus@felisadipiscingfringilla.edu'),
(26, 'Rutledge', 'Anastasia', '6538 Id Ave', '05 03 43 12 48', '05 70 43 61 23', 'Suspendisse.aliquet@congueturpis.net'),
(27, 'Elliott', 'Avram', 'Appartement 428-8488 Tortor Impasse', '03 84 49 77 34', '03 74 01 80 95', 'fames.ac.turpis@dictumeleifend.com'),
(28, 'Blevins', 'Rhiannon', '5822 Velit. Ave', '09 58 07 58 26', '04 52 45 17 68', 'magnis.dis@ullamcorper.net'),
(29, 'Jarvis', 'Chanda', '1124 Arcu Avenue', '07 31 73 65 71', '05 51 38 85 96', 'lorem@Aeneansedpede.com'),
(30, 'Davenport', 'Charles', '6010 Ligula. Rue', '08 18 08 52 00', '07 89 81 67 72', 'aliquam@temporlorem.com'),
(31, 'Alston', 'Giselle', 'CP 925, 9526 Egestas Avenue', '09 04 90 72 66', '08 14 02 58 38', 'in.dolor.Fusce@lacusEtiambibendum.ca'),
(32, 'Preston', 'Bianca', 'CP 605, 8645 Quisque Ave', '08 45 74 09 24', '06 57 96 69 35', 'et.rutrum@amet.ca'),
(33, 'Franco', 'Wayne', '8648 Lacinia Chemin', '05 75 78 81 59', '09 81 11 55 25', 'molestie.Sed.id@Vestibulumuteros.ca'),
(34, 'Sloan', 'Yetta', 'Appartement 195-1847 Mi. Ave', '05 49 48 64 55', '07 83 21 66 62', 'convallis.erat.eget@vulputatelacus.edu'),
(35, 'Mcmahon', 'Cassandra', 'CP 402, 1249 Malesuada Av.', '09 71 16 84 48', '07 74 02 71 68', 'Suspendisse@semperduilectus.org'),
(36, 'Stokes', 'Isabella', '133-5036 Risus. Rue', '07 43 02 29 69', '06 33 88 31 60', 'mattis.semper.dui@sagittis.net'),
(37, 'Mccarty', 'Sade', '3574 Praesent Chemin', '07 44 14 21 49', '08 23 58 65 78', 'convallis.convallis.dolor@Fuscemollis.edu'),
(38, 'Kim', 'Fuller', '726-2914 Egestas. Route', '01 51 96 40 04', '02 32 91 78 31', 'adipiscing.lacus@SuspendisseeleifendCras.com'),
(39, 'Gates', 'Quinn', 'Appartement 685-2573 Interdum Av.', '04 10 36 93 68', '07 09 18 92 86', 'Pellentesque.habitant@ataugue.org'),
(40, 'Massey', 'Olivia', '626-1302 Elit, Ave', '04 56 87 30 13', '03 73 52 17 65', 'Pellentesque@Suspendisseseddolor.edu'),
(41, 'Mcknight', 'Kelly', 'CP 732, 4806 Aenean Chemin', '06 49 59 83 16', '03 89 95 02 79', 'nisl.elementum.purus@nunc.ca'),
(42, 'Sykes', 'Connor', '896-5169 Magnis Av.', '09 91 08 58 64', '07 01 62 21 73', 'sit.amet@blanditviverra.org'),
(43, 'Lowe', 'Addison', '601-2346 Lorem Route', '08 53 16 61 40', '05 79 11 75 54', 'feugiat.Lorem@montesnasceturridiculus.org'),
(44, 'Moon', 'Aaron', 'Appartement 964-9170 A, Route', '02 43 58 11 72', '08 77 53 27 41', 'mollis.vitae.posuere@ametlorem.net'),
(45, 'Burris', 'Jeanette', '777-4453 A Avenue', '06 93 81 29 71', '01 07 81 57 37', 'ante@neque.org'),
(46, 'Payne', 'Reece', 'Appartement 926-7026 Lorem. Rue', '07 04 11 66 84', '08 54 84 72 83', 'amet.orci.Ut@massalobortis.edu'),
(47, 'Cervantes', 'Macaulay', 'Appartement 361-665 In, Avenue', '03 15 01 90 11', '07 57 89 44 12', 'ultricies.ornare@enimEtiamgravida.edu'),
(48, 'Erickson', 'Shad', '8086 Ligula. Impasse', '07 32 46 87 99', '09 77 86 59 51', 'Mauris@quisarcu.edu'),
(49, 'Hansen', 'Philip', '147-9105 Eget Route', '04 88 51 08 65', '07 72 92 28 21', 'mauris@Duisdignissimtempor.org'),
(50, 'Small', 'Steven', 'CP 673, 4123 Malesuada Rue', '05 15 83 83 83', '01 23 14 86 99', 'tellus@ligula.org'),
(51, 'Hogan', 'Katelyn', '6632 Metus. Impasse', '01 02 11 37 34', '08 58 99 43 33', 'turpis.Nulla@semegestasblandit.co.uk'),
(52, 'Cannon', 'Sonia', 'CP 215, 6878 Eu Impasse', '07 78 87 57 16', '09 89 95 96 37', 'tincidunt@luctus.com'),
(53, 'Matthews', 'Sara', '557 Sociis Route', '06 56 57 57 32', '07 48 48 34 48', 'sodales.purus@sapienAenean.org'),
(54, 'Baker', 'Emily', '9757 Aliquam, Ave', '08 60 23 88 08', '08 54 18 06 91', 'non.feugiat@euaccumsan.com'),
(55, 'Dillard', 'Ethan', '6644 Donec Impasse', '04 75 18 79 10', '05 75 69 90 10', 'faucibus.orci@vitaevelitegestas.com'),
(56, 'Page', 'Zahir', '6243 Eu Rd.', '09 12 54 20 21', '05 77 23 23 90', 'in.consectetuer@risusMorbi.edu'),
(57, 'Long', 'Griffith', '251-4932 Ligula. Avenue', '07 55 28 80 84', '09 16 40 44 13', 'metus@sapienmolestie.net'),
(58, 'Guzman', 'Hayden', '483-7668 Imperdiet Rue', '09 96 88 79 16', '04 30 73 12 27', 'tortor.Integer.aliquam@conubia.edu'),
(59, 'Booker', 'Iliana', 'CP 648, 7083 In Route', '03 69 77 54 04', '04 84 41 02 36', 'quam.Pellentesque.habitant@massa.com'),
(60, 'Larson', 'Nevada', 'CP 527, 9206 Magna. Avenue', '08 31 87 73 23', '07 71 06 66 96', 'ante.iaculis@ornareInfaucibus.co.uk'),
(61, 'Kim', 'Cathleen', '599-6504 Risus. Chemin', '01 93 34 91 80', '06 40 52 72 23', 'Integer@purussapiengravida.net'),
(62, 'Henderson', 'Arden', 'CP 313, 9456 Erat. Avenue', '06 78 62 56 46', '09 97 61 44 17', 'hendrerit.id.ante@Phasellusat.edu'),
(63, 'Salazar', 'Michelle', '1788 Egestas Av.', '05 54 46 24 18', '07 01 91 62 68', 'diam.Pellentesque@liberoatauctor.co.uk'),
(64, 'Snow', 'Brock', 'CP 561, 2574 Sed, Impasse', '08 85 91 30 20', '02 83 33 40 33', 'nibh@eutellus.org'),
(65, 'Fitzgerald', 'Abdul', '291-7169 Est, Ave', '02 47 76 26 67', '03 57 97 36 36', 'mi@in.org'),
(66, 'Talley', 'Brock', 'Appartement 895-4252 Varius. Route', '05 49 27 15 92', '09 37 55 64 77', 'orci@egestasadui.ca'),
(67, 'Beach', 'Darryl', '197-8002 Ultrices Impasse', '08 04 32 79 13', '03 57 75 34 07', 'magna.Suspendisse@netusetmalesuada.edu'),
(68, 'Barron', 'Alea', 'Appartement 863-7066 Pede Rue', '04 97 80 72 56', '05 67 81 13 25', 'libero@Donecsollicitudin.co.uk'),
(69, 'Morse', 'Richard', '4184 Massa Ave', '04 37 27 28 87', '08 38 96 57 69', 'metus.eu@fringillacursus.com'),
(70, 'Case', 'Merritt', 'Appartement 339-208 In Rue', '09 98 17 89 51', '04 63 47 75 26', 'turpis.egestas.Fusce@Namligulaelit.com'),
(71, 'Burch', 'Kristen', 'Appartement 441-5082 Porttitor Avenue', '04 23 09 67 01', '02 87 37 69 17', 'eros.turpis@Duisrisus.ca'),
(72, 'Parsons', 'Odysseus', 'CP 259, 5431 Sit Route', '08 28 82 33 82', '03 03 19 54 86', 'pede@Curae.co.uk'),
(73, 'Macdonald', 'Bernard', 'Appartement 869-3112 Massa. Avenue', '01 76 60 33 57', '04 12 89 96 79', 'diam.Sed@quisturpisvitae.net'),
(74, 'Brown', 'Lance', '565-9206 Dui Impasse', '05 86 14 85 10', '01 81 87 46 32', 'tortor.Nunc.commodo@euplacerat.ca'),
(75, 'Jacobson', 'Cain', 'CP 643, 5952 Suspendisse Chemin', '03 67 93 72 52', '05 50 03 40 11', 'lorem@odio.edu'),
(76, 'Baxter', 'Hilary', 'Appartement 130-4598 Pharetra. Rue', '05 48 86 96 36', '04 67 98 96 25', 'mollis.Duis.sit@faucibusMorbivehicula.co.uk'),
(77, 'Frazier', 'Stuart', '1221 Scelerisque Route', '06 87 28 15 83', '04 49 75 30 60', 'vitae@Craseu.co.uk'),
(78, 'Rojas', 'Julie', 'Appartement 533-5478 At, Route', '07 54 58 30 99', '06 62 77 04 05', 'velit.Cras@sitametorci.org'),
(79, 'Holman', 'Denton', '8904 Duis Ave', '07 96 11 16 61', '04 67 17 08 46', 'Etiam.gravida.molestie@Seddictum.ca'),
(80, 'Fisher', 'Danielle', 'Appartement 763-4974 Dapibus Chemin', '04 49 04 02 60', '05 10 13 95 33', 'mi.pede.nonummy@ultriciesdignissim.co.uk'),
(81, 'Cherry', 'Geraldine', 'CP 189, 9832 Dapibus Impasse', '05 11 43 89 87', '01 77 19 55 99', 'non@Vestibulum.org'),
(82, 'Good', 'Jada', 'Appartement 852-3835 Et, Route', '06 31 45 36 80', '01 18 90 59 72', 'eget@indolorFusce.org'),
(83, 'Kaufman', 'Hop', 'CP 647, 6680 Est Avenue', '08 12 73 05 05', '06 37 07 93 27', 'a.arcu@seddolorFusce.net'),
(84, 'Underwood', 'Rama', 'CP 274, 3763 Pede. Avenue', '07 17 92 18 33', '07 91 31 29 92', 'mattis.semper@velsapien.org'),
(85, 'Waters', 'Chava', '3733 Cursus Ave', '07 82 92 72 46', '03 58 05 71 61', 'dui.Suspendisse.ac@purusactellus.net'),
(86, 'Frost', 'Erich', 'Appartement 511-8564 Urna. Av.', '09 05 10 14 84', '06 90 40 18 39', 'libero.Morbi.accumsan@nulla.edu'),
(87, 'Craig', 'Bradley', '117 Sed Avenue', '05 09 56 33 37', '07 54 68 29 07', 'consectetuer.rhoncus@aarcuSed.org'),
(88, 'Bruce', 'Daphne', 'Appartement 593-9120 Vestibulum Rue', '08 42 53 72 56', '04 07 75 29 77', 'tempor.est.ac@pulvinararcu.org'),
(89, 'Morrow', 'Xandra', '1090 Libero. Ave', '04 65 74 53 32', '03 58 82 76 82', 'Suspendisse.eleifend.Cras@lacus.org'),
(90, 'Mckenzie', 'Charlotte', '818-7633 Sed Chemin', '05 61 95 87 18', '08 59 07 57 27', 'augue.eu.tellus@loremvehicula.edu'),
(91, 'Wagner', 'Alexandra', '3746 Non, Chemin', '05 11 92 53 34', '01 10 99 86 40', 'massa.Integer.vitae@maurisInteger.ca'),
(92, 'Burgess', 'Holly', '356-1525 Eu Chemin', '08 07 65 48 64', '04 04 33 00 21', 'amet.lorem.semper@felisullamcorperviverra.edu'),
(93, 'Williamson', 'Giselle', 'Appartement 315-8693 Libero Av.', '02 68 87 65 62', '02 05 57 38 53', 'ac.ipsum@sed.net'),
(94, 'Pollard', 'Lila', '502-6617 Nisl. Av.', '05 27 30 69 65', '08 21 74 70 06', 'metus.In@consequatdolorvitae.co.uk'),
(95, 'Bean', 'Kennedy', '569-8925 Eget Av.', '07 66 94 77 58', '08 90 52 09 31', 'nonummy.ac@odiosempercursus.edu'),
(96, 'Duke', 'Eden', '374-9873 Nunc Ave', '02 84 17 13 19', '01 78 36 02 68', 'in.cursus@aceleifend.co.uk'),
(97, 'Cantrell', 'Nadine', 'CP 846, 3282 Per Chemin', '08 71 73 47 67', '01 40 57 32 23', 'Proin.non.massa@Nullamut.ca'),
(98, 'Jimenez', 'Erin', '2598 Risus. Impasse', '06 11 60 70 85', '08 04 22 33 19', 'nec.tellus@non.com'),
(99, 'William', 'Kameko', 'CP 558, 6530 Tortor. Rd.', '07 64 02 77 98', '02 62 64 21 13', 'Cras.eget.nisi@Donecegestas.edu'),
(100, 'Baird', 'Colt', 'CP 825, 3960 Nunc, Rue', '04 16 83 48 47', '08 87 32 67 24', 'ultricies.ornare@maurisSuspendisse.net'),
(101, 'Luna', 'Jameson', 'Appartement 448-5969 Malesuada Chemin', '04 54 77 28 46', '07 76 86 39 52', 'et@temporest.org'),
(102, 'Marsh', 'Rachel', 'Appartement 781-1946 Aenean Avenue', '05 81 29 99 30', '04 77 98 64 88', 'placerat.orci.lacus@dolorFuscefeugiat.co.uk');

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE IF NOT EXISTS `employe` (
  `idEmploye` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `prenoms` varchar(45) DEFAULT NULL,
  `age` varchar(45) DEFAULT NULL,
  `adresse` varchar(45) DEFAULT NULL,
  `telephonePortable` varchar(45) DEFAULT NULL,
  `telephoneFixe` varchar(45) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `metier_idMetier` int(11) NOT NULL,
  `actif` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `employe`
--

INSERT INTO `employe` (`idEmploye`, `nom`, `prenoms`, `age`, `adresse`, `telephonePortable`, `telephoneFixe`, `mail`, `metier_idMetier`, `actif`) VALUES
(1, 'Jean-Jacques', 'Bijoutier', '42', NULL, NULL, NULL, NULL, 1, 1),
(2, 'Jean-Bob', 'Contrôleur', NULL, NULL, NULL, NULL, NULL, 2, 1),
(3, 'Jean-Louis', 'Fondeur', NULL, NULL, NULL, NULL, NULL, 3, 0),
(4, 'Jean-Louis', 'Polisseur', NULL, NULL, NULL, NULL, NULL, 4, 1),
(5, 'Jean-Jambon', 'Sertisseur', NULL, NULL, NULL, NULL, NULL, 5, 1),
(6, 'Jean-Léonide', 'Tailleur', NULL, NULL, NULL, NULL, NULL, 6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `etatBijoux`
--

CREATE TABLE IF NOT EXISTS `etatBijoux` (
  `idEtatBijoux` int(11) NOT NULL,
  `etatBijoux` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `etatBijoux`
--

INSERT INTO `etatBijoux` (`idEtatBijoux`, `etatBijoux`) VALUES
(1, 'Planifié'),
(2, 'En cours'),
(3, 'En attente du client'),
(4, 'En stock');

-- --------------------------------------------------------

--
-- Structure de la table `etatIntervention`
--

CREATE TABLE IF NOT EXISTS `etatIntervention` (
  `idEtatIntervention` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `etatIntervention`
--

INSERT INTO `etatIntervention` (`idEtatIntervention`, `description`) VALUES
(1, 'En attente'),
(2, 'Terminer'),
(3, 'En attente de validation'),
(4, 'Valider');

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

CREATE TABLE IF NOT EXISTS `intervention` (
  `idIntervention` int(11) NOT NULL,
  `bijoux_id` int(11) NOT NULL,
  `metier_idMetier` int(11) NOT NULL,
  `dateFin` date DEFAULT NULL,
  `etatIntervention_idetatIntervention` int(11) NOT NULL,
  `tempsPasser` int(11) DEFAULT NULL,
  `commentaire` varchar(45) DEFAULT NULL,
  `idemploye` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `intervention`
--

INSERT INTO `intervention` (`idIntervention`, `bijoux_id`, `metier_idMetier`, `dateFin`, `etatIntervention_idetatIntervention`, `tempsPasser`, `commentaire`, `idemploye`) VALUES
(1, 1, 4, '2015-05-27', 1, NULL, NULL, NULL),
(2, 1, 4, '2015-05-07', 4, 5, 'erdtgyf', 4),
(3, 2, 4, '2015-02-01', 2, 5, 'sdjh', 4),
(4, 5, 4, NULL, 1, NULL, NULL, NULL),
(5, 16, 3, NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `intervention_has_photos`
--

CREATE TABLE IF NOT EXISTS `intervention_has_photos` (
  `intervention_idIntervention` int(11) NOT NULL,
  `photos_idphotos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `materiau`
--

CREATE TABLE IF NOT EXISTS `materiau` (
  `idMateriau` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `prixAuKilo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `materiau_has_intervention`
--

CREATE TABLE IF NOT EXISTS `materiau_has_intervention` (
  `intervention_idIntervention` int(11) NOT NULL,
  `quantite` double NOT NULL,
  `materiau_idMateriau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `metier`
--

CREATE TABLE IF NOT EXISTS `metier` (
  `idMetier` int(11) NOT NULL,
  `nomMetier` varchar(45) DEFAULT NULL,
  `nomIntervention` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `metier`
--

INSERT INTO `metier` (`idMetier`, `nomMetier`, `nomIntervention`) VALUES
(1, 'Bijoutier', NULL),
(2, 'Contrôleur', 'Contrôle'),
(3, 'Fondeur', 'Fonte'),
(4, 'Polisseur', 'Polissage'),
(5, 'Sertisseur', 'Sertissage'),
(6, 'Tailleur', 'Taille');

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `idPhotos` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `emplacement` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `bijoux`
--
ALTER TABLE `bijoux`
  ADD PRIMARY KEY (`idBijoux`,`client_idclient`), ADD KEY `fk_bijoux_client_idx` (`client_idclient`), ADD KEY `fk_bijoux_etatBijoux1_idx` (`etatBijoux_idetatBijoux`);

--
-- Index pour la table `bijoux_has_photos`
--
ALTER TABLE `bijoux_has_photos`
  ADD PRIMARY KEY (`bijoux_id`,`photos_idphotos`), ADD KEY `fk_bijoux_has_photos_photos1_idx` (`photos_idphotos`), ADD KEY `fk_bijoux_has_photos_bijoux1_idx` (`bijoux_id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idClient`);

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`idEmploye`,`metier_idMetier`), ADD KEY `fk_employe_metier1_idx` (`metier_idMetier`);

--
-- Index pour la table `etatBijoux`
--
ALTER TABLE `etatBijoux`
  ADD PRIMARY KEY (`idEtatBijoux`);

--
-- Index pour la table `etatIntervention`
--
ALTER TABLE `etatIntervention`
  ADD PRIMARY KEY (`idEtatIntervention`);

--
-- Index pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD PRIMARY KEY (`idIntervention`,`bijoux_id`,`metier_idMetier`), ADD KEY `fk_intervention_metier1_idx` (`metier_idMetier`), ADD KEY `fk_intervention_bijoux1_idx` (`bijoux_id`), ADD KEY `fk_intervention_etatIntervention1_idx` (`etatIntervention_idetatIntervention`);

--
-- Index pour la table `intervention_has_photos`
--
ALTER TABLE `intervention_has_photos`
  ADD PRIMARY KEY (`intervention_idIntervention`,`photos_idphotos`), ADD KEY `fk_intervention_has_photos_photos1_idx` (`photos_idphotos`), ADD KEY `fk_intervention_has_photos_intervention1_idx` (`intervention_idIntervention`);

--
-- Index pour la table `materiau`
--
ALTER TABLE `materiau`
  ADD PRIMARY KEY (`idMateriau`);

--
-- Index pour la table `materiau_has_intervention`
--
ALTER TABLE `materiau_has_intervention`
  ADD PRIMARY KEY (`intervention_idIntervention`,`materiau_idMateriau`), ADD KEY `fk_materiau_has_intervention_intervention1_idx` (`intervention_idIntervention`), ADD KEY `fk_materiau_has_intervention_materiau1_idx` (`materiau_idMateriau`);

--
-- Index pour la table `metier`
--
ALTER TABLE `metier`
  ADD PRIMARY KEY (`idMetier`);

--
-- Index pour la table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`idPhotos`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bijoux`
--
ALTER TABLE `bijoux`
  MODIFY `idBijoux` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idClient` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT pour la table `employe`
--
ALTER TABLE `employe`
  MODIFY `idEmploye` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `etatBijoux`
--
ALTER TABLE `etatBijoux`
  MODIFY `idEtatBijoux` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `etatIntervention`
--
ALTER TABLE `etatIntervention`
  MODIFY `idEtatIntervention` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `intervention`
--
ALTER TABLE `intervention`
  MODIFY `idIntervention` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `materiau`
--
ALTER TABLE `materiau`
  MODIFY `idMateriau` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `metier`
--
ALTER TABLE `metier`
  MODIFY `idMetier` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `photos`
--
ALTER TABLE `photos`
  MODIFY `idPhotos` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `bijoux`
--
ALTER TABLE `bijoux`
ADD CONSTRAINT `fk_bijoux_client` FOREIGN KEY (`client_idclient`) REFERENCES `client` (`idClient`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_bijoux_etatBijoux1` FOREIGN KEY (`etatBijoux_idetatBijoux`) REFERENCES `etatBijoux` (`idEtatBijoux`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `bijoux_has_photos`
--
ALTER TABLE `bijoux_has_photos`
ADD CONSTRAINT `fk_bijoux_has_photos_bijoux1` FOREIGN KEY (`bijoux_id`) REFERENCES `bijoux` (`idBijoux`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_bijoux_has_photos_photos1` FOREIGN KEY (`photos_idphotos`) REFERENCES `photos` (`idPhotos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `employe`
--
ALTER TABLE `employe`
ADD CONSTRAINT `fk_employe_metier1` FOREIGN KEY (`metier_idMetier`) REFERENCES `metier` (`idMetier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `intervention`
--
ALTER TABLE `intervention`
ADD CONSTRAINT `fk_intervention_bijoux1` FOREIGN KEY (`bijoux_id`) REFERENCES `bijoux` (`idBijoux`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_intervention_etatIntervention1` FOREIGN KEY (`etatIntervention_idetatIntervention`) REFERENCES `etatIntervention` (`idEtatIntervention`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_intervention_metier1` FOREIGN KEY (`metier_idMetier`) REFERENCES `metier` (`idMetier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `intervention_has_photos`
--
ALTER TABLE `intervention_has_photos`
ADD CONSTRAINT `fk_intervention_has_photos_intervention1` FOREIGN KEY (`intervention_idIntervention`) REFERENCES `intervention` (`idIntervention`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_intervention_has_photos_photos1` FOREIGN KEY (`photos_idphotos`) REFERENCES `photos` (`idPhotos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `materiau_has_intervention`
--
ALTER TABLE `materiau_has_intervention`
ADD CONSTRAINT `fk_materiau_has_intervention_intervention1` FOREIGN KEY (`intervention_idIntervention`) REFERENCES `intervention` (`idIntervention`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_materiau_has_intervention_materiau1` FOREIGN KEY (`materiau_idMateriau`) REFERENCES `materiau` (`idMateriau`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
