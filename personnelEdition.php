<?php
session_start();
if (!isset($_SESSION['idMetier'])&& isset($_GET['idMetier'])) {
	$_SESSION['idMetier']=$_GET['idMetier'];
}

include_once("Menu.class.php");
include_once("Config.class.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edition personnel</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="style.css" rel="stylesheet" type="text/css"/>
	<style type="text/css">a:link{text-decoration:none}</style>

</head>
<body>
	<?php
	$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	$req = $pdo->prepare("SELECT nomMetier FROM metier WHERE idMetier= ?");
	$req->execute(array($_SESSION['idMetier']));
	$pdo=null;
	?>

	<div id='image'><p><a href="accueil.php"><img src="images/logo.png" alt="logo" /></a></p></div>
	<?php
	foreach  ($req as $row) {
		echo'<h1>'.$row['nomMetier'].'</h1>';
	}
	?>
	<?php


	Menu::display($_SESSION['idMetier']);
	if (isset($_POST["nom"])) {
		$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		$req = $pdo->prepare('INSERT INTO employe( nom, prenoms, age, adresse, telephonePortable, telephoneFixe, mail, metier_idMetier) VALUES (:nom, :prenom, :age, :adresse, :telPortable, :telFixe, :mail, :metier)') ;
		$req->bindParam(":nom",$_POST['nom']);
		$req->bindParam(":prenom",$_POST['prenom']);
		$req->bindParam(":age",$_POST['age']);
		$req->bindParam(":adresse",$_POST['adresse']);
		$req->bindParam(":telPortable",$_POST['telPortable']);
		$req->bindParam(":telFixe",$_POST['telFixe']);
		$req->bindParam(":mail",$_POST['mail']);
		$req->bindParam(":metier",$_POST['metier']);
		$req->execute();
		$req=null;

}elseif(isset($_POST["idEmploye"])){
$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		$req = $pdo->prepare('UPDATE employe SET nom=:nom,prenoms=:prenom,age=:age,adresse=:adresse,telephonePortable=:telPortable,telephoneFixe=telFixe,mail=mail,`metier_idMetier`=[value-9],`actif`=[value-10] WHERE 1') ;
		$req->bindParam(":nom",$_POST['nom']);
		$req->bindParam(":prenom",$_POST['prenom']);
		$req->bindParam(":age",$_POST['age']);
		$req->bindParam(":adresse",$_POST['adresse']);
		$req->bindParam(":telPortable",$_POST['telPortable']);
		$req->bindParam(":telFixe",$_POST['telFixe']);
		$req->bindParam(":mail",$_POST['mail']);
		$req->bindParam(":metier",$_POST['metier']);
		$req->execute();
		$req=null;

	}elseif(isset($_GET["idEmploye"])){
		?>
		<form method="post" action="personnelAjout.php" id="Formulaire">
			<h1>Edition d'un employé</h1>
			<label for="nom">Nom de l'employé :</label><br>
			<input type="text" name="nom" id="nom" required/>
			<br>
			<label for="prenom">Prénom de l'employé :</label><br>
			<input type="text" name="prenom" id="prenom" required/>
			<br>
			<label for="telFixe">Téléphone fixe de l'employé :</label><br>
			<input type="tel" name="telFixe" id="telFixe" />
			<br>
			<label for="telPortable">Téléphone portable de l'employé :</label><br>
			<input type="tel" name="telPortable" id="telPortable"/>
			<br>
			<label for="age">Âge de l'employé :</label><br>
			<input type="number" name="age" id="age" required/>
			<br>
			<label for="email">Adresse mail de l'employé :</label><br>
			<input type="mail" name="mail" id="mail" />
			<br>
			<label for="adresse">Adresse de l'employé :</label><br>
			<input type="text" name="adresse" id="adresse" required/>
			<br>


			<label for="metier">Quel est son métier ?</label><br>
			<select name="metier" id="metier" required>
				<?php
				$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
				$req = $pdo->prepare('SELECT idMetier, nomMetier FROM metier');
				$req->execute();
				foreach ($req as $row) {
					echo '<option valuse="'.$row['idMetier'].'">'.$row['nomMetier'].'</option>';
				}
				?>
			</select>
			<br>
			<input type="submit">
			<input type="reset">
		</form>
		<?php
	} else {
		?>
		<div id="cadre">
			<form method="post" action="personnelAjout.php" id="Formulaire">
				<h1>Ajout d'un employé</h1>
				<label for="nom">Nom de l'employé :</label><br>
				<input type="text" name="nom" id="nom" required/>
				<br>
				<label for="prenom">Prénom de l'employé :</label><br>
				<input type="text" name="prenom" id="prenom" required/>
				<br>
				<label for="telFixe">Téléphone fixe de l'employé :</label><br>
				<input type="tel" name="telFixe" id="telFixe" />
				<br>
				<label for="telPortable">Téléphone portable de l'employé :</label><br>
				<input type="tel" name="telPortable" id="telPortable"/>
				<br>
				<label for="age">Âge de l'employé :</label><br>
				<input type="number" name="age" id="age" required/>
				<br>
				<label for="email">Adresse mail de l'employé :</label><br>
				<input type="mail" name="mail" id="mail" />
				<br>
				<label for="adresse">Adresse de l'employé :</label><br>
				<input type="text" name="adresse" id="adresse" required/>
				<br>
				<label for="metier">Quel est son métier ?</label><br>
				<select name="metier" id="metier" required>
					<?php
					$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
					$req = $pdo->prepare('SELECT idMetier, nomMetier FROM metier');
					$req->execute();
					foreach ($req as $row) {
						echo '<option valuse="'.$row['idMetier'].'">'.$row['nomMetier'].'</option>';
					}?>
				</select>
				<br>
				<input type="submit">
				<input type="reset">
			</form>

		</div>
		<?php
	}
	?>


	</html>
