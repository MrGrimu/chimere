<?php
session_start ();
session_unset ();
session_destroy ();
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <link rel="stylesheet" href="index.css" />
  <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:300,400' rel='stylesheet' type='text/css'>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <title>Bijouterie Chimére </title>
<script type="text/javascript">
    function afficher(objet) {  
      if (objet.firstElementChild.id=="false"){
        objet.firstElementChild.style.display='inline';
        objet.firstElementChild.id='true';
      }
      else{
        objet.firstElementChild.style.display='none';
        objet.firstElementChild.id='false';
      }
  }

</script>
</head>
<body>
  <?php

    include_once("Config.class.php");
    $pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

  ?>
    <header class="row">
      <div class="col-lg-12">
        <h1>Bijouterie Chimère</h1>
      </div>
    </header>
  <div class="container">
    <!-- ------------------------------------------------------------------------------------------------------------------------------------- -->
    <div class="row"> <!-- Première ligne -->
      <div class="col-lg-4" onClick="afficher(this)" id="Polisseur"><!-- Carré du polisseur -->
        Polisseur
        <ul class="list-unstyled" id="false">
            <?php
   $req = $pdo->prepare('SELECT idEmploye,nom,prenoms,metier_idMetier FROM employe WHERE metier_idMetier="4" AND actif=1');
              $req->execute();
              foreach  ($req as $row) {
              $Noms = $row['prenoms'] ." " .$row['nom'];
                echo '<li><a href="accueil.php?idMetier='.$row['metier_idMetier'].'&idEmploye='.$row[0].'">'.$Noms.'</a></li>' ;
            }
            ?>
        </ul>
      </div>
      <div class="col-lg-offset-4 col-lg-4" onClick="afficher(this)" id="Fondeur">
        Fondeur
        <ul class="list-unstyled"  id="false">
            <?php
         $req = $pdo->prepare('SELECT idEmploye,nom,prenoms,metier_idMetier FROM employe WHERE metier_idMetier="3"AND actif=1');
              $req->execute();
              foreach  ($req as $row) {
              $Noms = $row['prenoms'] ." " .$row['nom'];
                echo '<li><a href="accueil.php?idMetier='.$row['metier_idMetier'].'&idEmploye='.$row[0].'">'.$Noms.'</a></li>' ;
            }
            ?>
        </ul>
      </div>
    </div>
    <!-- ------------------------------------------------------------------------------------------------------------------------------------- -->
    <div class="row"><!--  Deuxième ligne -->
      <div class="col-lg-offset-4 col-lg-4" onClick="afficher(this)" id="Bijoutier"><!-- Carré du Bijoutier -->
        Bijoutier
        <ul class="list-unstyled" id="false">
            <?php
           $req = $pdo->prepare('SELECT idEmploye,nom,prenoms,metier_idMetier FROM employe WHERE metier_idMetier="1"AND actif=1');
              $req->execute();
              foreach  ($req as $row) {
              $Noms = $row['prenoms'] ." " .$row['nom'];
                echo '<li><a href="accueil.php?idMetier='.$row['metier_idMetier'].'&idEmploye='.$row[0].'">'.$Noms.'</a></li>' ;
            }
            ?>
        </ul>
      </div>
    </div>
    <!-- ------------------------------------------------------------------------------------------------------------------------------------- -->
    <div class="row"><!--  Troisième ligne -->
      <div class="col-lg-offset-4 col-lg-4" onClick="afficher(this)" id="Controlleur"><!-- Carré du Contrôleur -->
        Contrôleur
        <ul class="list-unstyled"  id="false">
            <?php
            $req = $pdo->prepare('SELECT idEmploye,nom,prenoms,metier_idMetier FROM employe WHERE metier_idMetier="2"AND actif=1');
              $req->execute();
              foreach  ($req as $row) {
              $Noms = $row['prenoms'] ." " .$row['nom'];
                echo '<li><a href="accueil.php?idMetier='.$row['metier_idMetier'].'&idEmploye='.$row[0].'">'.$Noms.'</a></li>' ;
            }
            ?>
        </ul>
      </div>
    </div>
      <!-- ------------------------------------------------------------------------------------------------------------------------------------- -->
      <div class="row"><!--  Quatrème ligne -->
        <div class="col-lg-4" onClick="afficher(this)" id="Tailleur"><!-- Carré du Tailleur -->
          Tailleur
          <ul class="list-unstyled"  id="false">
              <?php
              $req = $pdo->prepare('SELECT idEmploye,nom,prenoms,metier_idMetier FROM employe WHERE metier_idMetier="6"AND actif=1');
              $req->execute();
              foreach  ($req as $row) {
                $Noms = $row['prenoms'] ." " .$row['nom'];
                echo '<li><a href="accueil.php?idMetier='.$row['metier_idMetier'].'&idEmploye='.$row[0].'">'.$Noms.'</a></li>' ;
              }
              ?>
          </ul>
        </div>
        <div class="col-lg-offset-4 col-lg-4" onClick="afficher(this)" id="Sertisseur">
          Sertisseur
          <ul class="list-unstyled"  id="false">
              <?php
              $req = $pdo->prepare('SELECT idEmploye,nom,prenoms,metier_idMetier FROM employe WHERE metier_idMetier="5"AND actif=1');
              $req->execute();
              foreach  ($req as $row) {
                $Noms = $row['prenoms'] ." " .$row['nom'];
                echo '<li><a href="accueil.php?idMetier='.$row['metier_idMetier'].'&idEmploye='.$row[0].'">'.$Noms.'</a></li>' ;
              }
              ?>
          </ul>
        </div>
    </div>
    <!-- ------------------------------------------------------------------------------------------------------------------------------------- -->
  </div>
</body>
