<!DOCTYPE html>
<html>
<head>
<style>
table {
    width: 100%;
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
    padding: 5px;
}

th {text-align: left;}
</style>
	<script src="ajax.js" type="text/javascript"></script>
</head>
<body>

<?php
    include_once("Config.class.php");
$q = intval($_GET['q']);

$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
$req = $pdo->prepare("SELECT idBijoux,nom,client_idclient,
                                            dateButtoir,dateSortieAtelier,
                                          dateCommande,tempsEstimer,
                                          prixCalculer,prixReel,
                                          etatBijoux_idetatBijoux,
                                          prixEstimer
                                           FROM bijoux WHERE idBijoux = '".$q."'");
		    $req->execute();



echo "<table>
<tr>
<th>ID</th>
<th>Nom</th>
<th>ID client
<th>Date butoire</th>
<th>Date de sortie de l'atelier</th>
<th>Date de commande</th>
<th>Temps estimé</th>
<th>Prix calculé</th>
<th>Prix réel</th>
<th>État bijou</th>
<th>Prix estimé</th>
</tr>";
    echo "<tr>";
	foreach($req as $row)  {
        echo '<th>' .$row[0] .  '</th>';
        echo '<th>'  .$row[1] .   '</th>';
        echo '<th>'  .$row[2] .   '</th>';
        echo '<th>'  .$row[3] .   '</th>';
        echo '<th>'  .$row[4] .   '</th>';
        echo '<th>'  .$row[5] .   '</th>';
        echo '<th>'  .$row[6] .   '</th>';
        echo '<th>' .$row[7] .    '</th>';
        echo '<th>'  .$row[8] .   '</th>';
        echo '<th>'   .$row[9] .  '</th>';
        echo '<th>' .$row[10] . '</th>';
		$id = $row[0];
        $nom = $row[1];
        $client_idclient = $row[2];
		$dateButtoir = $row[3];
		$dateSortieAtelier = $row[4];
		$dateCommande = $row[5];
		$tempsEstimer = $row[6];
		$prixCalculer = $row[7];
		$prixReel = $row[8];
		$etatBijoux_idetatBijoux = $row[9];
		$prixEstimer = $row[10];
	}

    echo "</tr>";
	echo "</table>";


		echo '<form method="post" action="editionBijoux.php" id="Formulaire">';
		echo '<h1>Modification du bijou nommé '.$nom.'</h1>';

	  	echo '<label for="id">ID du bijou :</label><br>';
		echo '<input type="nombre" disabled name="id" id="id" value="'.$id.'"/><br>';

		echo '<label for="nom">Nom du bijou :</label><br>';
		echo '<input type="text" name="nom" id="nom" value="'.$nom.'"/><br>';

		echo '<label for="client">Nom du client :</label><br>';
		//echo '<button type="button" onClick=""> Valider ce client	</button> ';
		echo '<div id="conteneurClient"><input type="text" name="client_idclient" id="client_idclient" value="'.$client_idclient.'" onkeyup=" recherche(this.value,this)"/>';
		echo '<div id="txtHint"></div><br></div>';


		echo '<label for="dateButtoir">Date butoire :</label><br>';
		echo '<input type="date" name="dateButtoir" id="dateButtoir" value="'.$dateButtoir.'"/><br>';

		echo '<label for="dateSortieAtelier">Date de sortie :</label><br>';
		echo '<input type="date" name="dateSortieAtelier" id="dateSortieAtelier" value="'.$dateSortieAtelier.'"/><br>';

		echo '<label for="dateCommande">Date de commande :</label><br>';
		echo '<input type="date" name="dateCommande" id="dateCommande"  value="'.$dateCommande.'"/><br>';

		echo '<label for="tmpEstime">Temps estimé en heures :</label><br>';
		echo '<input type="number" name="tempsEstimer" id="tempsEstimer" min="0" value="'.$tempsEstimer.'" /><br>';

		echo '<label for="prixCalculer">Prix calculé en € :</label><br>';
		echo '<input type="number" name="prixCalculer" id="prixCalculer" min="0" value="'.$prixCalculer.'"/><br>';

		echo '<label for="prixReel">Prix Reel en € :</label><br>';
		echo '<input type="number" name="prixReel" id="prixReel" min="0" value="'.$prixReel.'"/><br>';

		echo '<label for="etatBijoux_idetatBijoux"> etat du bijou  :</label><br>';
		echo '<select name="etatBijoux_idetatBijoux" id="etatBijoux_idetatBijoux" value="'.$etatBijoux_idetatBijoux.'">';

			$req = $pdo->prepare("SELECT idEtatIntervention, description FROM etatintervention ");
			$req->execute();
			foreach($req as $row)
			{
				echo '<option value='.$row[0].'>'.$row[1].'</option>';
			}

		echo '</select></br>';

		echo '<label for="prixEstime">Prix estimé en € :</label><br>';
		echo '<input type="number" name="prixEstimer" id="prixEstimer" min="0" value="'.$prixEstimer.'"/><br>';
		echo '<br>';
		echo '<br>';
		echo '<input type="submit">';
		echo '<input type="reset">';

		  ?>
</form>
</body>
</html>
