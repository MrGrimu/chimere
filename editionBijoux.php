<?php
session_start();
if (!isset($_SESSION['idMetier'])&& isset($_GET['idMetier'])) {
	$_SESSION['idMetier']=$_GET['idMetier'];
}
include_once("Menu.class.php");
include_once("Config.class.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edition bijou</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="style.css" rel="stylesheet" type="text/css"/>
	<style type="text/css">a:link{text-decoration:none}</style>
	<script src="ajax.js" type="text/javascript"></script>

</head>
<body>
	<?php

	$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	$req = $pdo->prepare("SELECT nomMetier FROM metier WHERE idMetier= ?");
	$req->execute(array($_SESSION['idMetier']));



	?>
	<div id='image'><p><a href="accueil.php"><img src="images/logo.png" alt="logo" /></a></p></div>
	<?php
	foreach  ($req as $row) {
	echo'<h1>'.$row['nomMetier'].'</h1>';
	}
	$req = null;
	?>
	<?php
	Menu::display($_SESSION['idMetier']);

	if (isset($_POST["nom"])) {
	$tabResult = array_values($_POST);
	$req = $pdo->prepare("SELECT idBijoux FROM bijoux WHERE nom='".$tabResult[0] .  " ' ")     ;
	$req->execute();
	$res = $req->fetch();
	$id=$res['idBijoux'];
	for ($i=0;$i<sizeof($tabResult);$i++)
	{
		if($tabResult[$i]=='')
		{
			$tabResult[$i]=null;
		}
	}
	$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	$req = $pdo->prepare('UPDATE bijoux SET nom=:nom,client_idclient=:client_idclient,dateButtoir=:dateButtoir,
											dateSortieAtelier=:dateSortieAtelier,dateCommande=:dateCommande,tempsEstimer=:tempsEstimer,
											prixCalculer=:prixCalculer,prixReel=:prixReel,etatBijoux_idetatBijoux=:etatBijoux_idetatBijoux,
											prixEstimer=:prixEstimer WHERE idBijoux='.$id);
											$client = substr($tabResult[1],3,3);
	$req->bindParam(":nom",		$tabResult[0]);
	$req->bindParam(":client_idclient",$client);
	$req->bindParam(":dateButtoir",$tabResult[2]	);
	$req->bindParam(":dateSortieAtelier",	$tabResult[3]);
	$req->bindParam(":dateCommande",	$tabResult[4]);
	$req->bindParam(":tempsEstimer",	$tabResult[5]);
	$req->bindParam(":prixCalculer",	$tabResult[6]);
	$req->bindParam(":prixReel",	$tabResult[7]);
	$req->bindParam(":etatBijoux_idetatBijoux",$tabResult[8]);
	$req->bindParam(":prixEstimer" ,	$tabResult[9]);
	$req->execute();
	$req=null;
	} else {

    $pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

		?>
		<label for="client">Choisissez un bijou a modifier:</label><br>


		<select name="client_idclient" id="client_idclient" onChange='MontrerBijoux(this.value)'>
			<?php
		  	$req = $pdo->prepare('SELECT idBijoux, nom from bijoux' );
		    $req->execute();
			foreach($req as $row)
			{
				echo '<option value='.$row[0].'>'.$row[1].'</option>';
			}
			$req=null;
			?>
			</select ></br>
			<div id="tabBij"><b></b></div>


<?php
	}
	?>

	</body>

	</html>
