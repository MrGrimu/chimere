function MontrerBijoux(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("tabBij").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getbijoux.php?q="+str,true);
        xmlhttp.send();
    }
}

function recherche(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "recherche.php?q=" + str , true);
        xmlhttp.send();
    }


}
function changerText(text,div) {   
     var form = document.forms['Formulaire'];
     form.elements['client_idclient'].value=text;
     div.parentElement.parentElement.style.display="none";     
}