<?php
session_start();

include_once("Menu.class.php");
include_once("Config.class.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Création d'un bijou</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="style.css" rel="stylesheet" type="text/css"/>
	<style type="text/css">a:link{text-decoration:none}</style>
	<script src="ajax.js" type="text/javascript"></script>
</head>
<?php
//echo $_GET['idMetier'];


/*if (session_is_registered()) {
	# code...
}*/
?>
<body>
	<?php

	$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	$req = $pdo->prepare("SELECT nomMetier FROM metier WHERE idMetier= ?");
	$req->execute(array($_SESSION['idMetier']));



	?>
	<div id='image'><p><a href="accueil.php"><img src="images/logo.png" alt="logo" /></a></p></div>
	<?php
	foreach  ($req as $row) {
	echo'<h1>'.$row['nomMetier'].'</h1>';
	}
	$req = null;
	?>
	<?php
	Menu::display($_SESSION['idMetier']);
		$tabResult = array_values($_POST);
		for ($i=0;$i<sizeof($tabResult);$i++)
		{
			if($tabResult[$i]=='')
			{
				$tabResult[$i]=null;
			}
		}
	if (isset($_POST["nom"])) {
	$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	$req = $pdo->prepare('INSERT INTO bijoux(nom,client_idclient,dateButtoir,dateCommande,tempsEstimer,prixEstimer,etatBijoux_idEtatBijoux ) VALUES (:nom, :client_idclient, :dateButtoir, :dateCommande, :tempsEstimer, :prixEstimer ,1)') ;
	$req->bindParam(":nom",$_POST['nom']);
	$client = substr($tabResult[1],3,3);
	echo 'test : '.$client;
	$req->bindParam(":client_idclient",$client);
	$req->bindParam(":dateButtoir",$_POST['dateButtoir']);
	$req->bindParam(":dateCommande",$_POST['dateCommande']);
	$req->bindParam(":tempsEstimer",$_POST['tempsEstimer']);
	$req->bindParam(":prixEstimer",$_POST['prixEstimer']);
	$req->execute();
	//var_dump ($req->errorInfo());
	//var_dump($req);
	$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$req=$pdo ->prepare("SELECT MAX(idBijoux) as valeur FROM bijoux");
	$req->execute();
	foreach ($req as $row) {
		$valeur=$row['valeur'];
		//var_dump($valeur);
	}
	$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	$req = $pdo ->prepare("INSERT INTO intervention (bijoux_id,metier_idMetier,etatIntervention_idetatIntervention)VALUES(:bijoux_id,:metier_idMetier,1) ");
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$req->bindParam(':bijoux_id',$valeur);
	$req->bindParam(':metier_idMetier',$_POST['metier']);
	$req->execute();


	$req=null;
	} else {
		?>
	<form method="post" action="bijouxCreation.php" id="Formulaire">
		<h1>Ajout d'un bijou</h1>
		<label for="nom">Nom du bijou :</label><br>
		<input type="text" name="nom" id="nom" required/><br>

		<label for="client">Nom du client :</label><br>


			<?php
		//echo '<button type="button" onClick=""> Valider ce client	</button> ';
		echo '<div id="conteneurClient"><input type="text" name="client_idclient" id="client_idclient" value="" onkeyup=" recherche(this.value)"/>';
		echo '<div id="txtHint"></div><br></div>';
			?>


		<label for="dateButtoir">Date buttoire :</label><br>
		<input type="date" name="dateButtoir" id="dateButtoir" required/><br>

		<label for="dateCommande">Date de commande :</label><br>
		<input type="date" name="dateCommande" id="dateCommande" required/><br>

		<label for="tmpEstime">Temps estimé en heures :</label><br>
		<input type="number" name="tempsEstimer" id="tempsEstimer" min="0" required/><br>

		<label for="prixEstime">Prix estimé en € :</label><br>
		<input type="number" name="prixEstimer" id="prixEstimer" min="0" required/><br>
		<br>
		<br>
		<label for="metier">Premier métier</label><br>
				<select name="metier" id="metier" required>

					<option value="3">Fondeur</option>
					<option value="4">Polisseur</option>
					<option value="5">Sertisseur</option>
					<option value="6">Tailleur</option>				</select>
		<input type="submit">
		<input type="reset">
		</form>
		<?php
	}
	?>
	</body>
	</html>
