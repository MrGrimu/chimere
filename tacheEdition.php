<?php
session_start();


include_once("Menu.class.php");
include_once("Config.class.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Edition tâche</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet"/>
</head>
<?php
//echo $_GET['idMetier'];


/*if (session_is_registered()) {
	# code...
}*/
?>
<body>
<?php

$pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
$req = $pdo->prepare("SELECT nomMetier FROM metier WHERE idMetier= ?");
$req->execute(array($_SESSION['idMetier']));



?>
<div id='image'><p><a href="accueil.php"><img src="images/logo.png" alt="logo" /></a></p></div>
<?php
foreach  ($req as $row) {
    echo'<h1>'.$row['nomMetier'].'</h1>';
}
?>
<?php
Menu::display($_SESSION['idMetier']);
if (!isset($_POST['tempsPasser'])) {
    ?>
    <form method="post" action="tacheEdition.php" id="Formulaire">
        <h1>Edition d'une tâche</h1>
        <label for="client">Nom du bijou :</label><br>
        <select name="bijouxId" id="client_idclient">
            <?php
            $req = $pdo->prepare("SELECT nom, idBijoux FROM bijoux ");
            $req->execute();
            foreach($req as $row)
            {
                echo '<option value='.$row[1].'>'.$row[0].'</option>';
            }
            ?>

        </select></br>
        <?php
        echo'<input type="hidden" name="idEmploye" value='.$_SESSION['idEmploye'].'>';
        echo'<input type="hidden" name="idMetier" value='.$_SESSION['idMetier'].'>';
        ?>
        <label for="etatIntervention_idetatIntervention">Etat intervention</label>
        <select name="etatIntervention_idetatIntervention">
            <option value="2">Terminé</option>
            <option value="3">A valider</option>
        </select>
        <br>
        <label for="tempsPasser">Temps passé (en heure)</label>
        <input type="number" name="tempsPasser">
        <br>
        <label for="commentaire">Commentaire</label>
        <textarea name="commentaire"></textarea>
        <br>
        <label for="dateFin">Date de fin</label>
        <input type="date" name="dateFin">
        <br>
        <input type="submit">
        <input type="reset">
    </form>
    <?php
} else {
    $pdo = new PDO("mysql:host=".config::SERVERNAME.";dbname=".config::DBNAME, config::USER, config::PASSWORD,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $req = $pdo->prepare("INSERT INTO `intervention`( `bijoux_id`, `metier_idMetier`, `dateFin`, `etatIntervention_idetatIntervention`, `tempsPasser`, `commentaire`, `idemploye`)
			VALUES (:bijoux_id,:metier_idMetier,:dateFin,:etatIntervention_idetatIntervention,:tempsPasser,:commentaire,:idEmploye)");
    $req ->bindParam(':bijoux_id',$_POST['bijouxId']);
    $req ->bindParam(':metier_idMetier',$_POST['idMetier']);
    $req ->bindParam(':dateFin',$_POST['dateFin']);
    $req ->bindParam(':etatIntervention_idetatIntervention',$_POST['etatIntervention_idetatIntervention']);
    $req ->bindParam(':tempsPasser',$_POST['tempsPasser']);
    $req ->bindParam(':commentaire',$_POST['commentaire']);
    $req ->bindParam(':idEmploye',$_POST['idEmploye']);
    $req->execute();
}

?>




</body>

</html>
